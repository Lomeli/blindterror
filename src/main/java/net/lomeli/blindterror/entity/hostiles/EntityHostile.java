package net.lomeli.blindterror.entity.hostiles;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.entity.EntityStats;
import net.lomeli.blindterror.entity.action.DamageSource;

public abstract class EntityHostile extends Entity {

    public EntityHostile() {
        super(0, 0, 0, 0, 0);
    }

    public EntityHostile(EntityStats attributes) {
        super(attributes);
    }

    public EntityHostile(int hp, int atk, int def, int eva, int spd) {
        super(hp, atk, def, eva, spd);
    }

    @Override
    public boolean takeDamage(DamageSource source, int amount) {
        switch (source.getDamageType()) {
            case DARK:
                if (isImmuneToDark())
                    return false;
                break;
            case LIGHT:
                if (isImmuneToLight())
                    return false;
                break;
        }
        return super.takeDamage(source, amount);
    }

    @Override
    public boolean isImmuneToFire() {
        return false;
    }

    public boolean isImmuneToDark() {
        return true;
    }

    public boolean isImmuneToLight() {
        return false;
    }
}
