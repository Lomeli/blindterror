package net.lomeli.blindterror.entity.hostiles;

public interface IHostile {
    boolean isImmuneToDark();

    boolean isImmuneToLight();
}
