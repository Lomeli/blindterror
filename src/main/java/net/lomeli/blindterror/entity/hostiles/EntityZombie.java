package net.lomeli.blindterror.entity.hostiles;

import net.lomeli.blindterror.entity.action.hostiles.BiteAction;
import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.items.Items;

import java.util.Random;

public class EntityZombie extends EntityHostile {
    private static final Random RAND = new Random();

    private static final int DROPS = 50;

    public EntityZombie(int hp, int atk, int def) {
        super(hp, atk, def, 2, 3);
    }

    @Override
    public void registerAttacks() {
        registerAttack(new BiteAction(0.15f));
    }

    @Override
    public ItemStack[] drops() {
        ItemStack[] drops = new ItemStack[RAND.nextInt(3)];

        if (drops.length > 0) {
            for (int i = 0; i < drops.length; i++) {
                int rand = RAND.nextInt(DROPS);
                if (rand < 3)
                    drops[i] = new ItemStack(Items.COMBAT_KNIFE);
                else if (rand < 4)
                    drops[i] = new ItemStack(Items.POCKET_KNIFE);
                else if (rand < 5)
                    drops[i] = new ItemStack(Items.MEDKIT);
                else if (rand < 10)
                    drops[i] = new ItemStack(Items.RUSTED_KNIFE);
                else if (rand < 25)
                    drops[i] = new ItemStack(Items.SMALL_MEDKIT);
            }
        }

        return drops;
    }

    @Override
    public String getUnlocalizedName() {
        return "entity.hostile.zombie";
    }
}
