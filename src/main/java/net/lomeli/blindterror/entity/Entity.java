package net.lomeli.blindterror.entity;

import net.lomeli.blindterror.entity.action.DamageSource;
import net.lomeli.blindterror.entity.action.IAction;
import net.lomeli.blindterror.items.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class Entity {
    private final List<IAction> actions;
    private final EntityStats entityStats;

    public Entity() {
        this(0, 0, 0, 0, 0);
    }

    public Entity(EntityStats attributes) {
        entityStats = attributes;
        actions = new ArrayList<>();
        this.registerAttacks();
    }

    public Entity(int hp, int atk, int def, int eva, int spd) {
        this(new EntityStats(hp, atk, def, eva, spd));
    }

    public boolean takeDamage(DamageSource source, int amount) {
        if (source.getDamageType() == DamageSource.DamageType.FIRE && isImmuneToFire())
            return false;
        this.getStats().takeDamage(amount);
        return true;
    }

    public abstract void registerAttacks();

    protected boolean registerAttack(IAction action) {
        return actions.add(action);
    }

    public abstract ItemStack[] drops();

    public abstract boolean isImmuneToFire();

    public EntityStats getStats() {
        return entityStats;
    }

    public List<IAction> getActions() {
        return actions;
    }

    public abstract String getUnlocalizedName();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return Objects.equals(getStats(), entity.getStats());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStats());
    }
}
