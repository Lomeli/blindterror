package net.lomeli.blindterror.entity.player;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.entity.action.player.ChargeAction;
import net.lomeli.blindterror.entity.action.player.PunchAction;
import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.util.StringUtil;

public class EntityPlayer extends Entity {
    public final PlayerInventory inventory = new PlayerInventory();
    private final String name;

    public EntityPlayer(String name, int hp, int atk, int def, int eva, int spd) {
        super(hp, atk, def, eva, spd);
        this.name = name;
    }

    @Override
    public void registerAttacks() {
        registerAttack(new PunchAction(0.25f));
        registerAttack(new ChargeAction(0.5f));
    }

    @Override
    public String getUnlocalizedName() {
        return StringUtil.isStringNullOrEmpty(name) ? "entity.player" : name;
    }

    @Override
    public boolean isImmuneToFire() {
        return false;
    }

    @Override
    public ItemStack[] drops() {
        return new ItemStack[0];
    }
}
