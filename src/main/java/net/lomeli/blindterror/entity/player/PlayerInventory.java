package net.lomeli.blindterror.entity.player;

import net.lomeli.blindterror.items.ItemStack;

import java.util.Arrays;

public class PlayerInventory {
    public static final int MAX_INVENTORY_SIZE = 5;
    private final ItemStack[] inventory = new ItemStack[MAX_INVENTORY_SIZE];

    public PlayerInventory() {
        Arrays.fill(inventory, ItemStack.EMPTY_STACK);
    }

    public boolean addItem(ItemStack stack) {
        if (stack != null && !stack.isEmpty()) {
            int firstEmptySlot = -1;
            for (int i = 0; i < MAX_INVENTORY_SIZE; i++) {
                ItemStack item = inventory[i];

                if (firstEmptySlot == -1 && item.isEmpty())
                    firstEmptySlot = i;

                if (ItemStack.areStacksEqual(item, stack)) {
                    item.setCount(item.getCount() + stack.getCount());
                    inventory[i] = item;
                    return true;
                }
            }

            if (firstEmptySlot != -1) {
                inventory[firstEmptySlot] = stack;
                return true;
            }
        }
        return false;
    }

    public ItemStack getSlotStack(int slot) {
        return slot >= 0 && slot < MAX_INVENTORY_SIZE ? inventory[slot] : ItemStack.EMPTY_STACK;
    }

    public void setSlotStack(int slot, ItemStack stack) {
        if (stack == null || slot < 0 || slot >= MAX_INVENTORY_SIZE)
            return;
        inventory[slot] = stack;
    }

    public int size() {
        return inventory.length;
    }

    public boolean isEmpty() {
        for (ItemStack stack : inventory) {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    public ItemStack[] getInventory() {
        return inventory;
    }
}
