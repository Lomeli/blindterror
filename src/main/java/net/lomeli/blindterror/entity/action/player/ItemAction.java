package net.lomeli.blindterror.entity.action.player;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.entity.action.DamageSource;
import net.lomeli.blindterror.entity.action.IAction;
import net.lomeli.blindterror.entity.player.EntityPlayer;
import net.lomeli.blindterror.items.ItemResult;
import net.lomeli.blindterror.items.ItemKnife;
import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.localization.I18n;

public class ItemAction implements IAction {
    private final int slot;

    public ItemAction(int slot) {
        this.slot = slot;
    }

    @Override
    public void doAction(Entity doer, Entity target) {
        if (doer instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) doer;
            ItemStack stack = player.inventory.getSlotStack(slot);
            if (stack.isEmpty()) return;

            ItemResult result = stack.onItemUsed(player);
            if (stack.getItem() instanceof ItemKnife) {

                if (result.getResult() == ItemResult.Result.SUCCESS) {
                    target.takeDamage(new DamageSource.EntityDamageSource(doer, DamageSource.DamageType.STAB),
                            (int) result.getValue());
                    TerminalWriter.yellowLn(I18n.translateKey("attack.knife",
                            I18n.translateKey(doer.getUnlocalizedName()),
                            I18n.translateKey(target.getUnlocalizedName()), result.getValue()));
                }
            } else {
                if (result.getResult() == ItemResult.Result.SUCCESS)
                    player.inventory.setSlotStack(slot, stack);

                //TODO: Other types of weapons?
            }
        }
    }

    @Override
    public AssetLocation getID() {
        return new AssetLocation("use_item");
    }
}
