package net.lomeli.blindterror.entity.action;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.util.assets.AssetLocation;

import java.util.Objects;

public class DamageSource {
    public static final DamageSource GENERIC = new DamageSource(DamageType.GENERIC);
    public static final DamageSource FIRE = new DamageSource(DamageType.FIRE);
    public static final DamageSource LIGHT = new DamageSource(DamageType.LIGHT);
    public static final DamageSource DARK = new DamageSource(DamageType.DARK);

    private final DamageType damageType;

    public DamageSource(DamageType damageType) {
        this.damageType = damageType;
    }

    public DamageType getDamageType() {
        return damageType;
    }

    public AssetLocation getDamageMessage() {
        return new AssetLocation(this.getDamageType().toString().toLowerCase());
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof DamageSource && getDamageType() == ((DamageSource) o).getDamageType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDamageType());
    }

    public String getUnlocalizedMessage() {
        return "attack." + damageType.localizationKey;
    }

    public static class EntityDamageSource extends DamageSource {
        private final Entity source;

        public EntityDamageSource(Entity source, DamageType damageType) {
            super(damageType);
            this.source = source;
        }

        public Entity getSource() {
            return source;
        }

        @Override
        public boolean equals(Object o) {
            return super.equals(o) && (o instanceof EntityDamageSource
                    && getSource().equals(((EntityDamageSource) o).getSource()));
        }

        @Override
        public int hashCode() {
            return super.hashCode() + getSource().hashCode();
        }
    }

    public enum DamageType {
        GENERIC("generic"),
        PUNCH("punch"),
        SHOT("shot"),
        BITE("bite"),
        STAB("stab"),
        SCRATCH("scratch"),
        FIRE("fire"),
        LIGHT("light"),
        DARK("dark");

        private final String localizationKey;

        DamageType(String localizationKey) {
            this.localizationKey = localizationKey;
        }

        public String getLocalizationKey() {
            return localizationKey;
        }
    }
}
