package net.lomeli.blindterror.entity.action;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.util.assets.AssetLocation;

public interface IAction {
    void doAction(Entity doer, Entity target);

    default boolean canDo(Entity entity) {
        return true;
    }

    AssetLocation getID();
}
