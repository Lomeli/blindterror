package net.lomeli.blindterror.entity.action.player;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.entity.action.DamageSource;
import net.lomeli.blindterror.entity.action.IAttack;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.localization.I18n;

import java.util.Random;

public class PunchAction implements IAttack {
    private static final Random rand = new Random();

    private final float criticalHitChance;

    public PunchAction(float criticalHitChance) {
        this.criticalHitChance = criticalHitChance;
    }

    @Override
    public void doAction(Entity doer, Entity target) {
        if (rand.nextInt(150) > target.getStats().getEvasion()) {
            int damage = doer.getStats().getAttack();
            if (rand.nextFloat() <= criticalHitChance)
                damage %= 2;
            damage -= target.getStats().getDefense();
            if (damage < 0)
                damage = 0;
            target.takeDamage(new DamageSource.EntityDamageSource(doer, DamageSource.DamageType.PUNCH), damage);
            TerminalWriter.yellowLn(I18n.translateKey("attack.punch",
                    I18n.translateKey(doer.getUnlocalizedName()), I18n.translateKey(target.getUnlocalizedName()), damage));
            if (damage < 1)
                TerminalWriter.redLn(I18n.translateKey("attack.failed", I18n.translateKey(target.getUnlocalizedName())));
        } else TerminalWriter.redLn(I18n.translateKey("attack.missed"), I18n.translateKey(doer.getUnlocalizedName()));
    }

    @Override
    public AssetLocation getID() {
        return new AssetLocation("punch");
    }
}
