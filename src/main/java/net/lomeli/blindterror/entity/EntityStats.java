package net.lomeli.blindterror.entity;


import java.util.Objects;

public class EntityStats {
    private int health;
    private int attack;
    private int defense;
    private int evasion;
    private int speed;

    private int maxHealth;

    protected EntityStats(int maxHealth, int attack, int defense, int evasion, int speed) {
        this.health = maxHealth;
        this.maxHealth = maxHealth;
        this.attack = attack;
        this.defense = defense;
        this.evasion = evasion;
        this.speed = speed;
    }


    public void takeDamage(int amount) {
        this.health -= Math.min(amount, this.health);
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int amount) {
        if (amount < 0)
            amount = 0;
        this.health = Math.min(amount, maxHealth);
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public int getEvasion() {
        return evasion;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public int getSpeed() {
        return speed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityStats that = (EntityStats) o;
        return getHealth() == that.getHealth() &&
                getAttack() == that.getAttack() &&
                getDefense() == that.getDefense() &&
                getEvasion() == that.getEvasion() &&
                getSpeed() == that.getSpeed() &&
                getMaxHealth() == that.getMaxHealth();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHealth(), getAttack(), getDefense(), getEvasion(), getSpeed(), getMaxHealth());
    }
}
