package net.lomeli.blindterror.util;

import java.util.Scanner;

public class StringUtil {
    public static boolean isStringNullOrEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    /**
     * @param scanner
     * @return 0 = no, 1 = yes, anything else = N/A
     */
    public static int userSelection(Scanner scanner) {
        int flag = -1;
        String input = scanner.nextLine();
        if (!StringUtil.isStringNullOrEmpty(input)) {
            switch (input.toLowerCase()) {
                case "y":
                case "yes":
                case "ok":
                case "okay":
                case "1":
                case "confirm":
                    flag = 1;
                    break;
                case "n":
                case "no":
                case "nah":
                case "cancel":
                case "0":
                case "back":
                    flag = 0;
                    break;
            }
        }
        return flag;
    }

    public static int safeParseInt(String str) {
        int i = -1;
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
        }
        return i;
    }
}
