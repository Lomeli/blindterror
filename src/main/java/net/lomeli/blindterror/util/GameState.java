package net.lomeli.blindterror.util;

import net.lomeli.blindterror.entity.player.EntityPlayer;

public class GameState {
    private static EntityPlayer player;

    public static EntityPlayer getPlayer() {
        return player;
    }

    public static void setPlayer(EntityPlayer player) {
        GameState.player = player;
    }

    public static void saveGameState(String saveName) {
        //TODO: Save game
    }
}
