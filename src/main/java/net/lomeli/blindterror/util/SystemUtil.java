package net.lomeli.blindterror.util;

import java.awt.*;
import java.net.URI;

public class SystemUtil {
    public static void openURL(String url) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI(url));
            } catch (Exception ex) {
                //noop
            }
        }
    }
}
