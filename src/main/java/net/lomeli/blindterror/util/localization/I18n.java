package net.lomeli.blindterror.util.localization;

import net.lomeli.blindterror.util.StringUtil;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.assets.ResourceUtil;

import java.util.HashMap;

public class I18n {
    private static final String US_ENGLISH_KEY = "en_US";
    private static HashMap<String, Language> regionLanguage = new HashMap<>();
    private static String currentRegion = US_ENGLISH_KEY;

    static {
        registerI18n(US_ENGLISH_KEY);
    }

    private static void registerI18n(String region) {
        registerI18n(ResourceUtil.getLocalizationText(new AssetLocation(region)));
    }

    private static void registerI18n(Language localization) {
        if (localization == null || StringUtil.isStringNullOrEmpty(localization.getRegion()) ||
                regionLanguage.containsKey(localization.getRegion()))
            return;
        regionLanguage.put(localization.getRegion(), localization);
    }

    public static String translateKey(String key, Object... args) {
        Language localization = getCurrentLocalization();
        if (localization == null) return key;
        String localizedText = localization.translate(key);
        if (!localizedText.equalsIgnoreCase(key) && args != null && args.length > 0)
            localizedText = String.format(localizedText, args);
        return localizedText;
    }

    public static void setRegionLanguage(String region) {
        if (StringUtil.isStringNullOrEmpty(region) || !regionLanguage.containsKey(region)) return;
        currentRegion = region;
    }

    private static Language getCurrentLocalization() {
        Language currentLocalization = regionLanguage.get(currentRegion);
        if (currentLocalization == null) currentLocalization = regionLanguage.get(US_ENGLISH_KEY);
        return currentLocalization;
    }
}
