package net.lomeli.blindterror.util.localization;

import java.util.HashMap;
import java.util.Map;

public class Language {
    public static final Language EMPTY = new Language();

    private HashMap<String, String> localizations;
    private final String region;

    public Language() {
        localizations = new HashMap<>();
        region = "en_US";
    }

    public Language(String region) {
        localizations = new HashMap<>();
        this.region = region;
    }

    public Language(String region, Map<String, String> localizations) {
        this(region);
        if (localizations != null)
            this.localizations.putAll(localizations);
    }

    public String translate(String key) {
        return localizations.containsKey(key) ? localizations.get(key) : key;
    }

    public String getRegion() {
        return region;
    }
}
