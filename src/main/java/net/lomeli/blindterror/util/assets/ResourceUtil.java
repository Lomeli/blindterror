package net.lomeli.blindterror.util.assets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.localization.Language;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;

public class ResourceUtil {
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().setLenient().create();

    private static InputStream getResource(AssetType assetType, AssetLocation location) {
        String loc = String.format(
                "/assets/%s/%s/%s", location.getId(), assetType.getFolder(), location.getResourceName()
        );
        if (!loc.toLowerCase().endsWith(assetType.getFileExt()))
            loc += assetType.getFileExt();
        return ResourceUtil.class.getResourceAsStream(loc);
    }

    public static Language getLocalizationText(AssetLocation location) {
        Language localization = Language.EMPTY;
        InputStream stream = getResource(AssetType.LOCALIZATION, location);
        if (stream != null) {
            String region = location.getResourceName();
            if (region.toLowerCase().endsWith(AssetType.LOCALIZATION.getFileExt()))
                region = region.substring(0, region.length() - 5);
            try {
                InputStreamReader streamReader = new InputStreamReader(stream);
                BufferedReader reader = new BufferedReader(streamReader);

                LinkedHashMap<String, String> map = GSON.fromJson(reader, LinkedHashMap.class);
                localization = new Language(region, map);

                reader.close();
                streamReader.close();
                stream.close();
            } catch (IOException ex) {
                TerminalWriter.redLn("[ERROR]: Could not open " + region + " localization file! Using EMPTY localization.");
                ex.printStackTrace();
            }
        }
        return localization;
    }
}
