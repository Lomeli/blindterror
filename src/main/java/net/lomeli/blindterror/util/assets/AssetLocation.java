package net.lomeli.blindterror.util.assets;

public class AssetLocation {
    private final String id;
    private final String resourceName;

    public AssetLocation(String id, String resourceName) {
        this.id = id;
        this.resourceName = resourceName;
    }

    public AssetLocation(String resourceName) {
        this("blindterror", resourceName);
    }

    public String getId() {
        return id;
    }

    public String getResourceName() {
        return resourceName;
    }

    @Override
    public String toString() {
        return id + ":" + resourceName;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof AssetLocation && (this == o ||
                (this.getId().equals(((AssetLocation) o).getId()) &&
                        this.getResourceName().equals(((AssetLocation) o).getResourceName())));
    }
}
