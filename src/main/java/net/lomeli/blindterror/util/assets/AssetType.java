package net.lomeli.blindterror.util.assets;

public enum AssetType {
    LOCALIZATION("lang", ".lang"), CONFIG("configs", ".cfg");
    private final String folder;
    private final String fileExt;

    AssetType(String folder, String fileExt) {
        this.folder = folder;
        this.fileExt = fileExt;
    }

    public String getFolder() {
        return folder;
    }

    public String getFileExt() {
        return fileExt;
    }
}
