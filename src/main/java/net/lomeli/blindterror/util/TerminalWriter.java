package net.lomeli.blindterror.util;

public class TerminalWriter {
    public static void println(Object obj, Object... args) {
        System.out.println(preparePrintln(obj, args));
    }

    public static void println() {
        println("");
    }

    public static void redLn(Object obj, Object... args) {
        println("\033[33m" + obj.toString() + "\033[0m", args);
    }

    public static void yellowLn(Object obj, Object... args) {
        println("\033[91m" + obj.toString() + "\033[0m", args);
    }

    public static void cyanLn(Object obj, Object... args) {
        println("\033[1;32m" + obj.toString() + "\033[0m");
    }

    public static void clearTerminal() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void slowPrint(Object obj, Object... args) {
        for (char ch : preparePrintln(obj, args).toCharArray()) {
            System.out.print(ch);
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                //noop
            }
        }
        System.out.println();
    }

    private static String preparePrintln(Object obj, Object... args) {
        String text = obj.toString();
        if (args != null && args.length > 0)
            text = String.format(text, args);
        return text;
    }
}