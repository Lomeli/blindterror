package net.lomeli.blindterror.items;

import net.lomeli.blindterror.entity.player.EntityPlayer;
import net.lomeli.blindterror.util.assets.AssetLocation;

import java.util.Objects;

public class Item {
    private final AssetLocation itemID;
    private int maxDamage, maxStackSize;
    private boolean takesDamage;

    public Item(AssetLocation itemID) {
        this.itemID = itemID;
    }

    public ItemResult onItemUsed(ItemStack stack, EntityPlayer player) {
        return new ItemResult(ItemResult.Result.NONE);
    }

    public Item setTakesDamage(boolean flag) {
        takesDamage = flag;
        return this;
    }

    public Item setMaxDamage(int amount) {
        if (amount < 0) amount = 0;
        maxDamage = amount;
        return this;
    }

    public Item setMaxStackSize(int amount) {
        if (amount < 1) amount = 1;
        maxStackSize = amount;
        return this;
    }

    public AssetLocation getItemID() {
        return itemID;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public int getMaxStackSize() {
        return maxStackSize;
    }

    public boolean takesDamage() {
        return takesDamage;
    }

    public String getUnlocalizedName() {
        return String.format("item.%1$s.%2$s", itemID.getId(), itemID.getResourceName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return getItemID().equals(item.getItemID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItemID(), getMaxDamage());
    }
}
