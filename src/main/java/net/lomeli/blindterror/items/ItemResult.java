package net.lomeli.blindterror.items;

import java.util.Objects;

public class ItemResult<T> {

    private T resultReturn;
    private Result result;

    public ItemResult(T resultReturn, Result result) {
        this.resultReturn = resultReturn;
        this.result = result;
    }

    public ItemResult(Result result) {
        this(null, result);
    }

    public T getValue() {
        return resultReturn;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemResult<?> that = (ItemResult<?>) o;
        return Objects.equals(getValue(), that.getValue()) &&
                getResult() == that.getResult();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue(), getResult());
    }

    public static enum Result {
        NONE, SUCCESS, FAIL;
    }
}