package net.lomeli.blindterror.items;

import net.lomeli.blindterror.entity.player.EntityPlayer;
import net.lomeli.blindterror.util.assets.AssetLocation;

public class ItemKnife extends Item {
    private final int weaponDamage;

    public ItemKnife(AssetLocation itemID, int weaponDamage, int maxDamage) {
        super(itemID);
        this.weaponDamage = weaponDamage;
        setMaxDamage(maxDamage);
        setMaxStackSize(1);
        setTakesDamage(true);
    }

    @Override
    public ItemResult onItemUsed(ItemStack stack, EntityPlayer player) {
        int dmg = player.getStats().getAttack() + ((ItemKnife) stack.getItem()).getWeaponDamage();
        stack.damageItem(1);
        return new ItemResult<Integer>(dmg, ItemResult.Result.SUCCESS);
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }
}
