package net.lomeli.blindterror.items;

import net.lomeli.blindterror.entity.player.EntityPlayer;

import java.util.Objects;

/**
 * ItemStacks should never have a null item!
 */
public class ItemStack {
    public static final ItemStack EMPTY_STACK = new ItemStack(Items.EMPTY);

    private Item item;
    private int count;
    private int damage;

    public ItemStack() {
        this(Items.EMPTY);
    }

    public ItemStack(Item item) {
        this(item, 1);
    }

    public ItemStack(Item item, int count) {
        this(item, count, 0);
    }

    public ItemStack(Item item, int count, int damage) {
        this.item = item == null ? Items.EMPTY : item;
        this.count = count;
        this.damage = damage;
    }

    public ItemResult onItemUsed(EntityPlayer player) {
        return this.item.onItemUsed(this, player);
    }

    public void decrement(int amount) {
        int decrement = Math.min(amount, count);
        count -= decrement;
        if (count <= 0) {
            count = 1;
            item = Items.EMPTY;
        }
    }

    public void damageItem(int amount) {
        int damage = Math.min(amount, item.getMaxDamage());
        this.damage += damage;
        if (this.damage == item.getMaxDamage()) {
            this.damage = 0;
            item = Items.EMPTY;
        }
    }

    public boolean isEmpty() {
        return this.item.equals(Items.EMPTY);
    }

    public Item getItem() {
        return item;
    }

    public int getCount() {
        return count;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setCount(int count) {
        if (count < 0) count = 0;
        if (count > item.getMaxStackSize()) count = item.getMaxStackSize();
        if (count == 0) this.item = Items.EMPTY;
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemStack stack = (ItemStack) o;
        return getCount() == stack.getCount() &&
                getDamage() == stack.getDamage() &&
                Objects.equals(getItem(), stack.getItem());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItem(), getCount(), getDamage());
    }

    public static boolean areStacksEqual(ItemStack stackA, ItemStack stackB) {
        return stackA.getItem().equals(stackB.getItem()) && stackA.getDamage() == stackB.getDamage();
    }
}
