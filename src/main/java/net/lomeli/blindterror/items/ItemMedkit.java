package net.lomeli.blindterror.items;

import net.lomeli.blindterror.entity.player.EntityPlayer;
import net.lomeli.blindterror.util.assets.AssetLocation;

import java.util.Random;

public class ItemMedkit extends Item {
    private static final Random rand = new Random();
    private int minAmount;
    private int maxAmount;

    public ItemMedkit(AssetLocation id, int minAmount, int maxAmount) {
        super(id);
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        setMaxDamage(0);
        setMaxStackSize(3);
        setTakesDamage(false);
    }

    @Override
    public ItemResult<Boolean> onItemUsed(ItemStack stack, EntityPlayer player) {
        player.getStats().setHealth(player.getStats().getHealth() + minAmount + rand.nextInt(maxAmount - minAmount + 1));
        stack.decrement(1);
        return new ItemResult<>(true, ItemResult.Result.SUCCESS);
    }
}
