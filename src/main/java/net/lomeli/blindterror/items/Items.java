package net.lomeli.blindterror.items;

import net.lomeli.blindterror.util.assets.AssetLocation;

import java.util.HashMap;

public class Items {
    private static final HashMap<AssetLocation, Item> REGISTERED_ITEMS = new HashMap<>();

    public static final Item EMPTY = registerItem(new Item(new AssetLocation("empty")));
    public static final Item SMALL_MEDKIT = registerItem(new ItemMedkit(new AssetLocation("small_medkit"), 1, 3));
    public static final Item MEDKIT = registerItem(new ItemMedkit(new AssetLocation("medkit"), 3, 5));
    public static final Item RUSTED_KNIFE = registerItem(new ItemKnife(new AssetLocation("rusted_knife"), 1, 3));
    public static final Item POCKET_KNIFE = registerItem(new ItemKnife(new AssetLocation("pocket_knife"), 2, 4));
    public static final Item COMBAT_KNIFE = registerItem(new ItemKnife(new AssetLocation("combat_knife"), 3, 5));

    public static Item registerItem(Item item) {
        if (REGISTERED_ITEMS.containsKey(item.getItemID()))
            return Items.EMPTY;
        REGISTERED_ITEMS.put(item.getItemID(), item);
        return item;
    }
}
