package net.lomeli.blindterror;

import net.lomeli.blindterror.scene.SceneManager;
import net.lomeli.blindterror.scene.menu.MainMenuScene;
import net.lomeli.blindterror.util.TerminalWriter;

import java.util.Scanner;

/**
 * Who's ready for a overly complicated first project? I am!
 */
public class BlindTerror {
    private static BlindTerror INSTANCE;

    public static void main(String[] args) {
        BlindTerror.getInstance().run();
    }

    public static BlindTerror getInstance() {
        if (INSTANCE == null)
            INSTANCE = new BlindTerror();
        return INSTANCE;
    }

    private boolean running;

    private BlindTerror() {
    }

    private void run() {
        running = true;

        Scanner scanner = new Scanner(System.in);
        SceneManager.enterScene(MainMenuScene.ID);
        while (running) {
            TerminalWriter.clearTerminal();
            SceneManager.getCurrentScene().playScene(scanner);
        }
        scanner.close();
    }

    public void stopGame() {
        running = false;
    }
}
