package net.lomeli.blindterror.scene;

import net.lomeli.blindterror.BlindTerror;
import net.lomeli.blindterror.util.assets.AssetLocation;

import java.util.Scanner;

public abstract class Scene {
    private final AssetLocation name;

    protected Scene(AssetLocation name) {
        this.name = name;
    }

    protected Scene(String id, String name) {
        this(new AssetLocation(id, name));
    }

    protected Scene(String name) {
        this(new AssetLocation(name));
    }

    /**
     * This plays out the current scene. If you don't transition to a new scene using {@link SceneManager#enterScene(AssetLocation)}
     * or end the game with {@link BlindTerror#stopGame()}, then this will repeat indefinitely.
     * <p>
     * DO NOT CLOSE THE SCANNER!
     *
     * @param scanner
     */
    public abstract void playScene(Scanner scanner);

    public AssetLocation getName() {
        return name;
    }

    protected void sleep() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            //TODO: Proper error catching here
        }
    }
}
