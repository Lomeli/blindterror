package net.lomeli.blindterror.scene;

import net.lomeli.blindterror.scene.interactive.ExplorationScene;
import net.lomeli.blindterror.scene.menu.HeroCreationScene;
import net.lomeli.blindterror.scene.menu.MainMenuScene;
import net.lomeli.blindterror.util.assets.AssetLocation;

import java.util.HashMap;

public class SceneManager {
    private static HashMap<AssetLocation, Scene> gameScenes = new HashMap<>();
    private static Scene previousScene;
    private static Scene currentScene;

    static {
        registerScene(new MainMenuScene());
        registerScene(new HeroCreationScene());
        registerScene(new ExplorationScene());
    }

    private static void registerScene(Scene scene) {
        if (gameScenes.containsKey(scene.getName()))
            return;
        gameScenes.put(scene.getName(), scene);
    }

    public static Scene getCurrentScene() {
        return currentScene;
    }

    public static Scene getPreviousScene() {
        return previousScene;
    }

    public static void enterScene(Scene scene) {
        previousScene = currentScene;
        currentScene = scene;
    }

    public static void enterScene(AssetLocation sceneName) {
        if (gameScenes.containsKey(sceneName)) {
            previousScene = currentScene;
            currentScene = gameScenes.get(sceneName);
        }
    }

    public static void enterScene(String id, String screenName) {
        enterScene(new AssetLocation(id, screenName));
    }
}
