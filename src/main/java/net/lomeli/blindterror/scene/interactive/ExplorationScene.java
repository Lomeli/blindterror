package net.lomeli.blindterror.scene.interactive;

import net.lomeli.blindterror.entity.hostiles.EntityHostile;
import net.lomeli.blindterror.entity.hostiles.EntityZombie;
import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.scene.Scene;
import net.lomeli.blindterror.scene.SceneManager;
import net.lomeli.blindterror.util.GameState;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.localization.I18n;
import net.lomeli.blindterror.world.CardinalDirections;
import net.lomeli.blindterror.world.MapRooms;
import net.lomeli.blindterror.world.Room;

import java.util.Random;
import java.util.Scanner;

public class ExplorationScene extends Scene {
    public static final AssetLocation ID = new AssetLocation("exploration");
    private static final Random rand = new Random();

    private Room currentRoom;
    private Room previousRoom;
    private boolean didEncounter;

    public ExplorationScene() {
        super(ID);
        this.currentRoom = MapRooms.GRAND_FOYER;
    }

    @Override
    public void playScene(Scanner scanner) {
        TerminalWriter.println(I18n.translateKey("scene.room.current_room",
                I18n.translateKey(GameState.getPlayer().getUnlocalizedName()),
                I18n.translateKey(currentRoom.getUnlocalizedName())));
        TerminalWriter.println(I18n.translateKey(currentRoom.getUnlocalizedDescription()));

        if (previousRoom != null) {
            TerminalWriter.println();
            TerminalWriter.println(I18n.translateKey("scene.room.previous_room",
                    I18n.translateKey(GameState.getPlayer().getUnlocalizedName()),
                    I18n.translateKey(previousRoom.getUnlocalizedName())));
        }

        if (!didEncounter && currentRoom.hasRandomEncounters() && rand.nextFloat() < 0.75f) {
            EntityHostile[] hostiles = new EntityHostile[rand.nextInt(3)];
            if (hostiles.length > 0) {
                for (int i = 0; i < hostiles.length; i++) {
                    hostiles[i] = new EntityZombie(10 + rand.nextInt(11), 10 + rand.nextInt(6), 1);
                }
                didEncounter = true;
                SceneManager.enterScene(new BattleScene(GameState.getPlayer(), hostiles));
                return;
            }
        }

        listRooms();

        TerminalWriter.println();
        TerminalWriter.println(I18n.translateKey("scene.room.action.select",
                I18n.translateKey(GameState.getPlayer().getUnlocalizedName())));

        listOptions();

        // Must cover EVERY possible option. EVERY. SINGLE. ONE!
        String input = scanner.nextLine();
        switch (input.trim().toLowerCase()) {
            case "up":
            case "north":
            case "go north":
            case "go up":
            case "go n":
            case "go u":
            case "head north":
            case "head n":
            case "head up":
            case "head u":
            case "travel north":
            case "travel n":
            case "travel up":
            case "travel u":
            case "cd north":
            case "cd n":
            case "cd up":
            case "cd u":
            case "n":
            case "u":
                changeRooms(CardinalDirections.NORTH);
                break;
            case "down":
            case "south":
            case "go south":
            case "go down":
            case "go s":
            case "go d":
            case "head south":
            case "head s":
            case "head down":
            case "head d":
            case "travel south":
            case "travel s":
            case "travel down":
            case "travel d":
            case "cd south":
            case "cd s":
            case "cd down":
            case "cd d":
            case "s":
            case "d":
                changeRooms(CardinalDirections.SOUTH);
                break;
            case "right":
            case "east":
            case "go east":
            case "go right":
            case "go e":
            case "go r":
            case "head east":
            case "head e":
            case "head right":
            case "head r":
            case "travel east":
            case "travel e":
            case "travel right":
            case "travel r":
            case "cd east":
            case "cd e":
            case "cd right":
            case "cd r":
            case "e":
            case "r":
                changeRooms(CardinalDirections.EAST);
                break;
            case "left":
            case "west":
            case "go west":
            case "go left":
            case "go w":
            case "go l":
            case "head west":
            case "head w":
            case "head left":
            case "head l":
            case "travel west":
            case "travel w":
            case "travel left":
            case "travel l":
            case "cd west":
            case "cd w":
            case "cd left":
            case "cd l":
            case "w":
            case "l":
                changeRooms(CardinalDirections.WEST);
                break;
            case "go back":
            case "back":
            case "return":
            case "previous":
            case "cd ..":
            case "cd ../":
                goBack();
                break;
            case "search for items":
            case "search room":
            case "search items":
            case "search item":
            case "search":
            case "items":
            case "look for items":
            case "look around":
            case "look":
            case "find items":
            case "find item":
            case "find":
                TerminalWriter.println(I18n.translateKey("scene.room.item.search"),
                        I18n.translateKey(GameState.getPlayer().getUnlocalizedName()));
                sleep();

                if (!currentRoom.isHiddenItemFound()) {
                    ItemStack stack = currentRoom.findItem();
                    if (stack.isEmpty())
                        TerminalWriter.println(I18n.translateKey("scene.room.item.nothing"));
                    else {
                        if (stack.isEmpty()) {
                            TerminalWriter.println(I18n.translateKey("scene.room.item.nothing"));
                        } else {
                            TerminalWriter.cyanLn(I18n.translateKey("scene.room.item.found",
                                    I18n.translateKey(GameState.getPlayer().getUnlocalizedName()),
                                    I18n.translateKey(stack.getItem().getUnlocalizedName())));

                            if (!GameState.getPlayer().inventory.addItem(stack))
                                TerminalWriter.println(I18n.translateKey("scene.room.item.full_inventory"));
                        }
                    }
                } else
                    TerminalWriter.println(I18n.translateKey("scene.room.item.nothing"));

                sleep();
                break;
        }
    }

    private void listOptions() {
        if (currentRoom.getNorth() != null && currentRoom.getNorth() != previousRoom) {
            TerminalWriter.println(I18n.translateKey("scene.room.action.go_direction",
                    I18n.translateKey(CardinalDirections.NORTH.getUnlocalizedName())));
        }
        if (currentRoom.getSouth() != null && currentRoom.getSouth() != previousRoom) {
            TerminalWriter.println(I18n.translateKey("scene.room.action.go_direction",
                    I18n.translateKey(CardinalDirections.SOUTH.getUnlocalizedName())));
        }
        if (currentRoom.getEast() != null && currentRoom.getEast() != previousRoom) {
            TerminalWriter.println(I18n.translateKey("scene.room.action.go_direction",
                    I18n.translateKey(CardinalDirections.EAST.getUnlocalizedName())));
        }
        if (currentRoom.getWest() != null && currentRoom.getWest() != previousRoom) {
            TerminalWriter.println(I18n.translateKey("scene.room.action.go_direction",
                    I18n.translateKey(CardinalDirections.WEST.getUnlocalizedName())));
        }

        if (previousRoom != null)
            TerminalWriter.println(I18n.translateKey("scene.room.action.go_back"));

        TerminalWriter.println(I18n.translateKey("scene.room.action.search_item"));
    }

    private void listRooms() {
        int count = 0;
        if (currentRoom.getNorth() != null && currentRoom.getNorth() != previousRoom) {
            TerminalWriter.println(I18n.translateKey("scene.room.connecting_rooms.first",
                    I18n.translateKey(currentRoom.getNorth().getUnlocalizedName()),
                    I18n.translateKey(CardinalDirections.NORTH.getUnlocalizedName())));
            count++;
        }
        if (currentRoom.getSouth() != null && currentRoom.getNorth() != previousRoom) {
            String msg = "scene.room.connecting_rooms." + (count == 0 ? "first" : "following");
            TerminalWriter.println(I18n.translateKey(msg,
                    I18n.translateKey(currentRoom.getSouth().getUnlocalizedName()),
                    I18n.translateKey(CardinalDirections.SOUTH.getUnlocalizedName())));
            count++;
        }
        if (currentRoom.getEast() != null && currentRoom.getNorth() != previousRoom) {
            String msg = "scene.room.connecting_rooms." + (count == 0 ? "first" : "following");
            TerminalWriter.println(I18n.translateKey(msg,
                    I18n.translateKey(currentRoom.getEast().getUnlocalizedName()),
                    I18n.translateKey(CardinalDirections.EAST.getUnlocalizedName())));
            count++;
        }
        if (currentRoom.getWest() != null && currentRoom.getNorth() != previousRoom) {
            String msg = "scene.room.connecting_rooms." + (count == 0 ? "first" : "following");
            TerminalWriter.println(I18n.translateKey(msg, I18n.
                            translateKey(currentRoom.getWest().getUnlocalizedName()),
                    I18n.translateKey(CardinalDirections.WEST.getUnlocalizedName())));
        }
    }

    private void goBack() {
        if (previousRoom != null) {
            Room temp = previousRoom;
            previousRoom = currentRoom;
            currentRoom = temp;
            didEncounter = false;
        }
    }

    private void changeRooms(CardinalDirections direction) {
        Room nextRoom = null;
        switch (direction) {
            case NORTH:
                nextRoom = currentRoom.getNorth();
                break;
            case SOUTH:
                nextRoom = currentRoom.getSouth();
                break;
            case EAST:
                nextRoom = currentRoom.getEast();
                break;
            case WEST:
                nextRoom = currentRoom.getWest();
                break;
        }

        if (nextRoom != null) {
            previousRoom = currentRoom;
            currentRoom = nextRoom;
            didEncounter = false;
        }
    }
}
