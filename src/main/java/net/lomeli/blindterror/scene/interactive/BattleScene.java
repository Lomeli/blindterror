package net.lomeli.blindterror.scene.interactive;

import net.lomeli.blindterror.entity.Entity;
import net.lomeli.blindterror.entity.action.IAction;
import net.lomeli.blindterror.entity.action.player.ItemAction;
import net.lomeli.blindterror.entity.hostiles.EntityHostile;
import net.lomeli.blindterror.entity.player.EntityPlayer;
import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.scene.Scene;
import net.lomeli.blindterror.scene.SceneManager;
import net.lomeli.blindterror.scene.menu.MainMenuScene;
import net.lomeli.blindterror.util.StringUtil;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.localization.I18n;

import java.util.*;

public class BattleScene extends Scene {
    public static final AssetLocation ID = new AssetLocation("battle");
    private static final Random RAND = new Random();

    private EntityPlayer player;
    private List<Entity> turnOrder;
    private List<EntityHostile> enemies;
    private List<ItemStack> drops;

    private int turn;
    private int selectedTarget;

    private BattlePhase battlePhase;
    private IAction queuedAttack;

    public BattleScene(EntityPlayer player, EntityHostile... hostiles) {
        super(ID);
        this.player = player;
        this.turnOrder = new ArrayList<>();
        this.enemies = new ArrayList<>();
        this.drops = new ArrayList<>();
        this.turnOrder.add(player);
        this.turnOrder.addAll(Arrays.asList(hostiles));
        this.turnOrder.sort((entity1, entity2) ->
                Integer.compare(entity2.getStats().getSpeed(), entity1.getStats().getSpeed())
        );
        this.enemies.addAll(Arrays.asList(hostiles));
        this.battlePhase = BattlePhase.BATTLE_START;
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Override
    public void playScene(Scanner scanner) {
        String playerName = I18n.translateKey(player.getUnlocalizedName());
        switch (battlePhase) {
            case BATTLE_START:
                // Tell player if they have 1 or more opponents
                String msg;
                if (enemies.size() > 1)
                    msg = I18n.translateKey("scene.battle.enemy.multiple", playerName, enemies.size(),
                            I18n.translateKey(enemies.get(0).getUnlocalizedName()));
                else
                    msg = I18n.translateKey("scene.battle.enemy.one", playerName,
                            I18n.translateKey(enemies.get(0).getUnlocalizedName()));
                TerminalWriter.yellowLn(msg);
                sleep();

                battlePhase = BattlePhase.SELECT_ACTION;
                break;
            case SELECT_ACTION:
                // Display player's possible actions ask for their selection
                TerminalWriter.println(I18n.translateKey("scene.battle.turn_count", turn++ + 1));
                renderHP();
                TerminalWriter.println(I18n.translateKey("scene.battle.action", playerName));
                int count = 1;
                for (IAction action : player.getActions()) {
                    TerminalWriter.println("%o: %s", count++, I18n.translateKey("attack." +
                            action.getID().getResourceName() + ".name"));
                }
                if (!player.inventory.isEmpty())
                    TerminalWriter.println(I18n.translateKey("scene.battle.action.items", count++));

                // Put player's selection in attack queue
                String input = scanner.nextLine();
                int selection = StringUtil.safeParseInt(input) - 1;
                if (selection >= 0 && selection <= player.getActions().size()) {
                    if (!player.inventory.isEmpty() && selection == player.getActions().size())
                        battlePhase = BattlePhase.USE_ITEM;
                    else
                        queuedAttack = player.getActions().get(selection);
                }

                // If there's only one target, resolve everyone's actions. Otherwise, ask player to choose a target
                if (queuedAttack != null) {
                    battlePhase = BattlePhase.RESOLVE_ACTIONS;
                    if (enemies.size() > 1)
                        battlePhase = BattlePhase.SELECT_TARGET;
                }
                break;
            case SELECT_TARGET:
                // Display which enemies are available and ask the player to target one.
                TerminalWriter.println(I18n.translateKey("scene.battle.select_enemy"));
                for (int i = 0; i < enemies.size(); i++) {
                    Entity hostile = enemies.get(i);
                    TerminalWriter.println("%o: %s - HP %o/%o", i + 1,
                            I18n.translateKey(hostile.getUnlocalizedName()), hostile.getStats().getHealth(),
                            hostile.getStats().getMaxHealth());
                }
                String targetInput = scanner.nextLine();
                int targetSelection = StringUtil.safeParseInt(targetInput) - 1;
                if (targetSelection >= 0 && targetSelection <= enemies.size()) {
                    selectedTarget = targetSelection;
                    battlePhase = BattlePhase.RESOLVE_ACTIONS;
                }
                break;
            case RESOLVE_ACTIONS:
                // We resolve everyone's actions for the turn

                // We use an iterator so we can remove enemies when they die from the loop
                Iterator<Entity> it = turnOrder.iterator();
                while (it.hasNext()) {
                    Entity entity = it.next();

                    if (entity == player) {
                        // Have the player perform their queued action
                        Entity target = enemies.get(selectedTarget);
                        queuedAttack.doAction(player, target);
                        queuedAttack = null;
                        // Display a message if the player killed their target
                        if (target.getStats().getHealth() <= 0) {
                            TerminalWriter.println(I18n.translateKey("death.hostile",
                                    I18n.translateKey(target.getUnlocalizedName()), playerName));
                        }
                        selectedTarget = 0;
                    } else if (entity.getStats().getHealth() > 0) {
                        // The enemy will perform a random action on the player
                        entity.getActions().get(RAND.nextInt(entity.getActions().size())).doAction(entity, player);

                        // If the action kills the player, go straight to the GAME_OVER case
                        if (player.getStats().getHealth() < 1) {
                            battlePhase = BattlePhase.GAME_OVER;
                            break;
                        }
                    }

                    // Remove entities from turn order and enemy list if they die
                    if (enemies.contains(entity) && entity.getStats().getHealth() < 1) {
                        drops.addAll(Arrays.asList(entity.drops()));
                        it.remove();
                        enemies.remove(entity);

                        // If there are no more enemies in the enemy list, the player has won.
                        if (enemies.size() == 0) {
                            battlePhase = BattlePhase.BATTLE_END;
                            break;
                        }
                    }
                }
                renderHP();
                sleep();
                if (battlePhase != BattlePhase.GAME_OVER) {
                    if (enemies.size() > 0)
                        battlePhase = BattlePhase.SELECT_ACTION;
                    else
                        battlePhase = BattlePhase.BATTLE_END;
                }
                break;
            case BATTLE_END:
                // The player won the battle
                TerminalWriter.cyanLn(I18n.translateKey("scene.battle.win", playerName));
                for (ItemStack stack : drops) {
                    if (player.inventory.addItem(stack))
                        TerminalWriter.cyanLn(I18n.translateKey("scene.battle.item.pickup",
                                I18n.translateKey(player.getUnlocalizedName()),
                                stack.getCount(),
                                I18n.translateKey(stack.getItem().getUnlocalizedName())));
                }
                SceneManager.enterScene(ExplorationScene.ID);
                reset();
                sleep();
                break;
            case GAME_OVER:
                // The player died, return to the title screen.
                TerminalWriter.redLn(I18n.translateKey("death.player", playerName,
                        I18n.translateKey(enemies.get(0).getUnlocalizedName())));
                TerminalWriter.slowPrint(I18n.translateKey("death.player.game_over", playerName,
                        I18n.translateKey(enemies.get(0).getUnlocalizedName())));
                sleep();
                SceneManager.enterScene(MainMenuScene.ID);
                reset();
                break;
            case USE_ITEM:
                // Display all items in a player's inventory
                TerminalWriter.cyanLn(I18n.translateKey("scene.battle.use_item"));
                for (int i = 0; i < player.inventory.size(); i++) {
                    ItemStack stack = player.inventory.getSlotStack(i);
                    // Skip any empty itemstacks
                    if (!stack.isEmpty()) {
                        StringBuilder itemEntry = new StringBuilder();
                        itemEntry.append(String.format("%o: %s", i + 1,
                                I18n.translateKey(stack.getItem().getUnlocalizedName())));
                        if (stack.getItem().takesDamage())
                            itemEntry.append(String.format(" %1$o/%2$o", stack.getDamage(), stack.getItem().getMaxDamage()));
                        else if (stack.getCount() > 1)
                            itemEntry.append(" ").append(stack.getCount());

                        TerminalWriter.println(itemEntry.toString());
                    }
                }
                TerminalWriter.println(I18n.translateKey("scene.battle.item.back"));

                // Take the player's item selection or if they want to go back
                String itemInput = scanner.nextLine();
                int itemSelection = StringUtil.safeParseInt(itemInput) - 1;

                if (itemInput.toLowerCase().equals("back") || itemSelection == -1)
                    battlePhase = BattlePhase.SELECT_ACTION;

                // I really don't like this if statement.
                if (itemSelection >= 0 && itemSelection < player.inventory.size() &&
                        !player.inventory.getSlotStack(itemSelection).isEmpty()) {
                    queuedAttack = new ItemAction(itemSelection);
                    battlePhase = BattlePhase.RESOLVE_ACTIONS;
                }
                break;
        }
    }

    private void renderHP() {
        float healthPercentage = (float) player.getStats().getHealth() / (float) player.getStats().getMaxHealth();
        int barWidth = 50;
        StringBuilder output = new StringBuilder("HP: [\033[91m");

        int pos = Math.round(barWidth * healthPercentage);
        for (int i = 0; i < barWidth; ++i) {
            if (i <= pos) output.append('█');
            else output.append('-');
        }

        output.append(String.format("\033[0m] %1$s/%2$s", player.getStats().getHealth(), player.getStats().getMaxHealth()));
        TerminalWriter.println(output.toString());
    }

    private void reset() {
        turnOrder.clear();
        enemies.clear();
        drops.clear();

        turn = 0;
        selectedTarget = 0;

        battlePhase = BattlePhase.BATTLE_START;
        queuedAttack = null;
    }

    private enum BattlePhase {
        BATTLE_START, BATTLE_END, SELECT_ACTION, RESOLVE_ACTIONS, GAME_OVER, SELECT_TARGET, USE_ITEM
    }
}
