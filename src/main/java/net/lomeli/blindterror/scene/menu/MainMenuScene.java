package net.lomeli.blindterror.scene.menu;

import net.lomeli.blindterror.BlindTerror;
import net.lomeli.blindterror.scene.Scene;
import net.lomeli.blindterror.scene.SceneManager;
import net.lomeli.blindterror.util.SystemUtil;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.localization.I18n;

import java.util.Scanner;

public class MainMenuScene extends Scene {
    public static final AssetLocation ID = new AssetLocation("main_menu");

    private boolean duck;

    public MainMenuScene() {
        super(ID);
    }

    @Override
    public void playScene(Scanner scanner) {
        TerminalWriter.cyanLn(I18n.translateKey("title.logo"));

        String newGame = I18n.translateKey("scene.mainmenu.newgame");
        String loadGame = I18n.translateKey("scene.mainmenu.loadgame");
        String quit = I18n.translateKey("scene.mainmenu.quit");

        TerminalWriter.println(I18n.translateKey("scene.mainmenu.selectoption"));
        TerminalWriter.println(newGame);
        TerminalWriter.println(loadGame);
        TerminalWriter.println(quit);

        String input = scanner.nextLine();
        switch (input.trim().toLowerCase()) {
            case "1":
            case "new":
            case "newgame":
            case "new game":
                SceneManager.enterScene(HeroCreationScene.ID);
                break;
            case "2":
            case "loadgame":
            case "load game":
                TerminalWriter.println("");
                //TODO: Transition to next scene
                break;
            case "3":
            case "quit":
            case "quit()":
            case "q":
            case "exit":
            case "exit()":
            case ":q":
            case ":q!":
                TerminalWriter.cyanLn(I18n.translateKey("scene.mainmenu.quit.message"));
                BlindTerror.getInstance().stopGame();
                break;
            case "duck":
                String msg = "";
                if (!duck) {
                    duck = true;
                    TerminalWriter.redLn("[ERROR]: " + I18n.translateKey("duck"));
                    SystemUtil.openURL("https://www.youtube.com/watch?v=SwxdBiazu8M");
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    TerminalWriter.redLn("[ERROR]: " + I18n.translateKey("duck.run", input));
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
        }
    }
}
