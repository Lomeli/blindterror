package net.lomeli.blindterror.scene.menu;

import net.lomeli.blindterror.entity.hostiles.EntityZombie;
import net.lomeli.blindterror.entity.player.EntityPlayer;
import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.items.Items;
import net.lomeli.blindterror.scene.interactive.BattleScene;
import net.lomeli.blindterror.scene.Scene;
import net.lomeli.blindterror.scene.SceneManager;
import net.lomeli.blindterror.scene.interactive.ExplorationScene;
import net.lomeli.blindterror.util.GameState;
import net.lomeli.blindterror.util.StringUtil;
import net.lomeli.blindterror.util.TerminalWriter;
import net.lomeli.blindterror.util.assets.AssetLocation;
import net.lomeli.blindterror.util.localization.I18n;

import java.util.Scanner;

public class HeroCreationScene extends Scene {
    public static final AssetLocation ID = new AssetLocation("hero_creation_scene");

    private static final int STARTING_POINTS = 50;
    private static final String[] STAT_NAMES = {"health", "attack", "defense", "evasion", "speed"};

    private boolean finished;
    private String name;
    private int[] enteredStats = new int[5];
    private int points = STARTING_POINTS;
    private int position = -1;

    public HeroCreationScene() {
        super(ID);
    }

    @Override
    public void playScene(Scanner scanner) {
        if (finished) {
            TerminalWriter.slowPrint(I18n.translateKey("scene.charcreator.continue"));
            sleep();
            TerminalWriter.clearTerminal();
            TerminalWriter.slowPrint(I18n.translateKey("scene.charcreator.survivalhorror"));
            sleep();
            TerminalWriter.clearTerminal();
            TerminalWriter.slowPrint(I18n.translateKey("scene.charcreator.goodluck"));
            sleep();

            GameState.setPlayer(new EntityPlayer(name, enteredStats[0], enteredStats[1], enteredStats[2], enteredStats[3],
                    enteredStats[4]));
            GameState.getPlayer().inventory.addItem(new ItemStack(Items.SMALL_MEDKIT, 2));
            finished = false;

            SceneManager.enterScene(ExplorationScene.ID);
            //SceneManager.enterScene(new BattleScene(GameState.getPlayer(), new EntityZombie(20, 15, 1)));
        } else {
            TerminalWriter.cyanLn(I18n.translateKey("scene.charcreator.title"));
            int input;
            switch (position) {
                case -1:
                    TerminalWriter.println(I18n.translateKey("scene.charcreator.enteryourname"));
                    String name = scanner.nextLine();
                    if (!StringUtil.isStringNullOrEmpty(name)) {
                        this.name = name;
                        position++;
                    }
                    break;
                case 5:
                    TerminalWriter.println(I18n.translateKey("scene.charcreator.areyousure"));
                    TerminalWriter.println(I18n.translateKey("scene.charcreator.areyousure.stats", this.name,
                            enteredStats[0], enteredStats[1], enteredStats[2], enteredStats[3], enteredStats[4]));
                    input = StringUtil.userSelection(scanner);
                    if (input == 1) {
                        if (points != 0)
                            position++;
                        else
                            reset(true);
                    } else if (input == 0)
                        reset();
                    break;
                case 6:
                    TerminalWriter.yellowLn(I18n.translateKey("scene.charcreator.areyousure.points", points));
                    input = StringUtil.userSelection(scanner);
                    if (input == 1)
                        reset(true);
                    else if (input == 0)
                        reset();
                    break;
                default:
                    if (position < enteredStats.length)
                        setStat(STAT_NAMES[position], scanner);
            }
        }
    }

    private void reset(boolean finished) {
        this.finished = finished;
        points = STARTING_POINTS;
        position = -1;
    }

    private void reset() {
        reset(false);
    }

    private void setStat(String stat, Scanner scanner) {
        TerminalWriter.println(I18n.translateKey("scene.charcreator.points", points));
        TerminalWriter.println(I18n.translateKey("scene.charcreator." + stat));
        int input = StringUtil.safeParseInt(scanner.nextLine());
        if (input >= 0 && input <= points) {
            enteredStats[position] = input;
            points -= input;
            position++;
        }
    }
}
