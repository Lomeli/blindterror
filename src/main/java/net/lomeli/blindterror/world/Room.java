package net.lomeli.blindterror.world;

import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.util.assets.AssetLocation;

public class Room {
    private Room north, south, east, west;
    private AssetLocation roomID;
    private ItemStack hiddenItem = ItemStack.EMPTY_STACK;
    private boolean found, randomEncounters;

    public Room(AssetLocation name, ItemStack stack, boolean randomEncounters) {
        this.roomID = name;
        if (stack != null)
            this.hiddenItem = stack;
        this.randomEncounters = randomEncounters;
    }

    public Room(String name, ItemStack stack, boolean randomEncounters) {
        this(new AssetLocation(name), stack, randomEncounters);
    }

    public Room(AssetLocation name, boolean randomEncounters) {
        this(name, ItemStack.EMPTY_STACK, randomEncounters);
    }

    public Room(String name, boolean randomEncounters) {
        this(new AssetLocation(name), randomEncounters);
    }

    public ItemStack findItem() {
        found = true;
        return getHiddenItem();
    }

    public boolean hasRandomEncounters() {
        return randomEncounters;
    }

    public Room getNorth() {
        return north;
    }

    public Room getSouth() {
        return south;
    }

    public Room getEast() {
        return east;
    }

    public Room getWest() {
        return west;
    }

    public Room[] getConnectedRooms() {
        return new Room[]{north, south, east, west};
    }

    public void setNorth(Room north) {
        this.north = north;
    }

    public void setHiddenItem(ItemStack hiddenItem) {
        this.hiddenItem = hiddenItem;
    }

    public void setSouth(Room south) {
        this.south = south;
    }

    public void setEast(Room east) {
        this.east = east;
    }

    public void setWest(Room west) {
        this.west = west;
    }

    public String getUnlocalizedName() {
        return String.format("room.%s.%s", roomID.getId(), roomID.getResourceName());
    }

    public String getUnlocalizedDescription() {
        return getUnlocalizedName() + ".description";
    }

    public ItemStack getHiddenItem() {
        return hiddenItem;
    }

    public boolean isHiddenItemFound() {
        return found;
    }
}
