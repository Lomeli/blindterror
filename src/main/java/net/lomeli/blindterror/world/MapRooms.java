package net.lomeli.blindterror.world;

import net.lomeli.blindterror.items.ItemStack;
import net.lomeli.blindterror.items.Items;

public class MapRooms {
    public static final Room GRAND_FOYER = new Room("grand_foyer", false);
    public static final Room DINNING_ROOM = new Room("dinning_room", true);
    public static final Room WEST_HALLWAY = new Room("west_hallway", true);
    public static final Room BATHROOM = new Room("west_bathroom", true);
    public static final Room MASTER_BEDROOM = new Room("master_bedroom", false);

    static {
        connectRooms(GRAND_FOYER, DINNING_ROOM, CardinalDirections.WEST);
        connectRooms(DINNING_ROOM, WEST_HALLWAY, CardinalDirections.NORTH);
        connectRooms(WEST_HALLWAY, BATHROOM, CardinalDirections.NORTH);
        connectRooms(WEST_HALLWAY, MASTER_BEDROOM, CardinalDirections.WEST);

        GRAND_FOYER.setHiddenItem(new ItemStack(Items.SMALL_MEDKIT));
        DINNING_ROOM.setHiddenItem(new ItemStack(Items.POCKET_KNIFE));
        BATHROOM.setHiddenItem(new ItemStack(Items.MEDKIT));
        MASTER_BEDROOM.setHiddenItem(new ItemStack(Items.COMBAT_KNIFE));
    }

    private static void connectRooms(Room room1, Room room2, CardinalDirections room1Door) {
        switch (room1Door) {
            case NORTH:
                room1.setNorth(room2);
                room2.setSouth(room1);
                break;
            case SOUTH:
                room1.setSouth(room2);
                room2.setNorth(room1);
                break;
            case EAST:
                room1.setEast(room2);
                room2.setWest(room1);
                break;
            case WEST:
                room1.setWest(room2);
                room2.setEast(room1);
        }
    }
}
