package net.lomeli.blindterror.world;

public enum CardinalDirections {
    NORTH("north"), SOUTH("south"), EAST("east"), WEST("west");

    private final String name;

    CardinalDirections(String name) {
        this.name = name;
    }

    public CardinalDirections getOpposite() {
        return this == NORTH ? SOUTH : this == SOUTH ? NORTH : this == EAST ? WEST : EAST;
    }

    public String getUnlocalizedName() {
        return "room.blindterror.directions." + name;
    }
}
